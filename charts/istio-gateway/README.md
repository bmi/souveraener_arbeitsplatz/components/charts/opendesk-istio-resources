<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# istio-gateway

A Helm chart for deploying an istio gateway

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-istio-resources https://gitlab.opencode.de/api/v4/projects/1386/packages/helm/stable
helm install my-release --version 2.0.2 opendesk-istio-resources/istio-gateway
```

### Install via OCI Registry

```console
helm repo add opendesk-istio-resources oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources
helm install my-release --version 2.0.2 opendesk-istio-resources/istio-gateway
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| annotations | object | `{}` | Define custom Gateway annotations. |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.domain | string | `"example.com"` | Define ingress/virtualservice domain. |
| global.hosts | object | `{}` | Define ingress/virtualservice host. Results in "https://{{ univentionCorporateServer }}.{{ domain }}" |
| nameOverride | string | `""` | String to partially override release name. |
| tls.enabled | bool | `true` | Enable HTTPS (Port 443) with TLS certificate. |
| tls.httpsRedirect | bool | `true` | Redirect all traffic from HTTP to HTTPS. |
| tls.minProtocolVersion | string | `"TLSV1_2"` | TLS protocol versions.  "TLS_AUTO" => "Automatically choose the optimal TLS version." "TLSV1_0" => "TLS version 1.0" "TLSV1_1" => "TLS version 1.1" "TLSV1_2" => "TLS version 1.2" "TLSV1_3" => "TLS version 1.3"  |
| tls.mode | string | `"SIMPLE"` | TLS modes enforced by the proxy  Choose kind of TLS mode  "PASSTHROUGH" => The SNI string presented by the client will be used as the match criterion in a VirtualService TLS                  route to determine the destination service from the service registry. "SIMPLE" => Secure connections with standard TLS semantics. "MUTUAL" => Secure connections to the downstream using mutual TLS by presenting server certificates for             authentication. "AUTO_PASSTHROUGH" => Similar to the passthrough mode, except servers with this TLS mode do not require an                       associated VirtualService to map from the SNI value to service in the registry. The                       destination details such as the service/subset/port are encoded in the SNI value. The proxy                       will forward to the upstream (Envoy) cluster (a group of endpoints) specified by the SNI                       value. This server is typically used to provide connectivity between services in disparate L3                       networks that otherwise do not have direct connectivity between their respective endpoints.                       Use of this mode assumes that both the source and the destination are using Istio mTLS to                       secure traffic. "ISTIO_MUTUAL" => Secure connections from the downstream using mutual TLS by presenting server certificates for                   authentication. Compared to Mutual mode, this mode uses certificates, representing gateway                   workload identity, generated automatically by Istio for mTLS authentication. When this mode is                   used, all other fields in TLSOptions should be empty. |
| tls.secretName | string | `""` | The name of the kubernetes secret which contains a TLS private key and certificate. Hint: This secret is not created by this chart and must be provided. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
