## [2.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources/compare/v2.0.1...v2.0.2) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([74ea48a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources/commit/74ea48a3ce53b77e2ba7c10e7237bf96e7dd44b1))

## [2.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources/compare/v2.0.0...v2.0.1) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([e337081](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources/commit/e33708133b0df39bd61ed7e051c68338def3ca30))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/istio-ressources/compare/v1.2.0...v2.0.0) (2023-09-27)


### Bug Fixes

* **istio-gateway:** Remove default gateway in global.hosts ([917781a](https://gitlab.souvap-univention.de/souvap/tooling/charts/istio-ressources/commit/917781a5af59499cb09c8d6b8e0bc00165a7d808))


### BREAKING CHANGES

* **istio-gateway:** Set global.hosts to empty dict

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/istio-ressources/compare/v1.1.2...v1.2.0) (2023-09-20)


### Features

* Add README and apply latest CI tooling ([73284dc](https://gitlab.souvap-univention.de/souvap/tooling/charts/istio-ressources/commit/73284dc964b505e03f0eac35335770544d53e403))
